hashtag_bot
---

I'm figuring out how to use ecto.

Initially this will be a rest api that manages hashtags and intelligently generates batches for use in posts for different accounts.

At some point it would be cool to have this function as a phoenix web app

---

Current goals:

[] data model

[] basic api

[] documentation for design (very high level)
